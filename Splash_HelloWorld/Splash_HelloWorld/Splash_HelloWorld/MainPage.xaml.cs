﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Splash_HelloWorld
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            AbsoluteLayout layout = new AbsoluteLayout();

            Label label1 = new Label();

            label1.Text = "Simpe list of names";
            label1.TextColor = Color.Red;
            label1.FontSize = 40;
            label1.FontAttributes = FontAttributes.Bold;
            label1.HorizontalOptions = LayoutOptions.Center;           
            layout.Children.Add(label1);

            ListView simpleListViewOfNames = new ListView();

            var names = new List<string>
            {
                "Andrew Johnson",
                "George Washington ",
                "James Sanderlan",
                "Harry Mayson",
                "Sibil Hauntington",
                "Sherill Mayson",
                "Daliya Gilespy",
                "Monthy Python",
                "Arnold Schwarzenegger",
                "Justin Bieber (damn it!)",
                "Andrew Johnson",
                "George Washington ",
                "James Sanderlan",
                "Harry Mayson",
                "Sibil Hauntington",
                "Sherill Mayson",
                "Daliya Gilespy",
                "Monthy Python",
                "Arnold Schwarzenegger",
                "Justin Bieber (damn it!)"
            };

            simpleListViewOfNames.ItemsSource = names;
            layout.Children.Add(simpleListViewOfNames);

           
            Button button = new Button
            {
                Text = "Click me!"
            };
            button.Clicked += OnButtonClicked;          



            layout.Children.Add(button, new Point(280, 600));
           
                    
            Content = layout;
          
        }

        private void OnButtonClicked(object sender, EventArgs e)
        {
            
        }
    }
}
